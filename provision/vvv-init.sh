#!/bin/sh

if [[ ! -e /srv/www/motorksites/internetmotors/htdocs/provisioning.lock ]]; then
    echo "Installing Multisite Network website..."

    noroot wp core download
    noroot wp core config

    # This mess is needed to create the rules in wp-config.php
    noroot wp db create
    noroot wp core multisite-install

    # WordPress.org
    noroot wp plugin install wordpress-importer --force
    noroot wp plugin install customizer-export-import --force
    noroot wp plugin install widget-importer-exporter --force
    noroot wp plugin install wordpress-seo --force
    noroot wp plugin install user-switching --force
    noroot wp plugin install force-regenerate-thumbnails --force
    noroot wp plugin install debug-bar --force
    noroot wp plugin install query-monitor --force

    # Activation
    noroot wp plugin activate wordpress-importer --network
    noroot wp plugin activate customizer-export-import --network
    noroot wp plugin activate widget-importer-exporter --network
    noroot wp plugin activate wordpress-seo --network
    noroot wp plugin activate user-switching --network
    noroot wp plugin activate force-regenerate-thumbnails --network
    noroot wp plugin activate debug-bar --network
    noroot wp plugin activate query-monitor --network


    noroot wp rewrite structure '/%postname%'
    touch ../htdocs/provisioning.lock
else
    # Let the user know the good news
    echo "Multisite site now installed";
fi
